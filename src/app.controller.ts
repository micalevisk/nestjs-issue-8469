import { Header, Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  @Header('Content-Type', 'application/pdf')
  testBug() {
    throw new Error()
  }
}
