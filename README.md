```bash
npm ci

# watch mode
export PORT=3000
npm run start:dev

# production mode
npm run start:prod


# In another shell
curl "localhost:$PORT"
```
